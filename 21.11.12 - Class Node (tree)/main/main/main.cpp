﻿#include <iostream>
#include <map>
#include "myNode.h"
using namespace std;

int main()
{
    myNode a("Bratsk");
    myNode b("Irkutsk");
    myNode c("Kras");
    myNode d("Novosib");

    a.connect(b, 5);
    d.connect(c, 7);
    b.connect(d, 3);
    if (a.searchDepth(c)) {
      cout << "YES" << endl;
    } else {
      cout << "NO" << endl;
    };

    myNode A1("1");
    myNode A2("2");
    myNode A3("3");
    myNode A4("4");
    myNode A5("5");
    myNode A6("test");
    A1.connect(A2, 10);
    A1.connect(A3, 30);
    A1.connect(A4, 50);
    A1.connect(A5, 10);
    A3.connect(A5, 10);
    A4.connect(A2, 40);
    A4.connect(A3, 20);
    A5.connect(A1, 10);
    A5.connect(A3, 10);
    A5.connect(A4, 30);
    int ans = A1.searchDijkstra(A3);
    cout << ans;

    return 0;
}