#include "myNode.h"
#include <map>
//#include <vector>
//#include <stdlib.h>
#include <iostream>
using namespace std;

myNode::myNode() {
	value = "none";
}

myNode::myNode(string new_value) {
	value = new_value;
}

myNode::~myNode() {
	value = "";
  connections.clear();
}

string myNode::getValue() {
	return value;
}

map<string, int> myNode::getConnections() {
	return connections;
}

map<string, myNode*> myNode::getConnections2() {
	return connections2;
}

void myNode::connect(myNode& node, int wei) {
	connections[node.getValue()] = wei;
  connections2[node.getValue()] = &node;
}

void myNode::disconnect(myNode node) {
	connections.erase(node.getValue());
  connections2.erase(node.getValue());
}

void myNode::print_connections() {
  for (auto it = connections.begin(); it != connections.end(); it++) {
    cout << it->first << ": " << it->second << endl;
  }
}

bool vec_find(vector<string> been, string b) {
	for (auto it = been.begin(); it != been.end(); it++) {
		if (*(it) == b) {
			return true;
		}
	}
	return false;
}

bool myNode::searchDepth2(vector<string> been, myNode& node, string finding) {
  if (node.getConnections().size() > 0){
    map<string, int> cons = node.getConnections();
    map<string, myNode*> cons2 = node.getConnections2();
    auto it2 = cons2.begin();
    for (auto it = cons.begin(); it != cons.end(); it++) {
      if (!(vec_find(been, it->first))) {
        been.push_back(it->first);
        if (it->first == finding) {
          return true;
        }
        else {
          if (searchDepth2(been, *(it2->second), finding)) {
            return true;
          }
        }
      }
      if (it2 != cons2.end()) {
        it2++;
      }
    }
  }
  return false;
}

bool myNode::searchDepth(myNode node) {
  if (connections.size() > 0) {
    vector<string> been;
    auto it2 = connections2.begin();
    for (auto it = connections.begin(); it != connections.end(); it++) {
      if (!(vec_find(been, it->first))) {
        been.push_back(it->first);
        if (it->first == node.getValue()) {
          return true;
        }
        else {
          if (searchDepth2(been, *(it2->second), node.getValue())) {
            return true;
          }
        }
      }
      if (it2 != connections2.end()) {
        it2++;
      }
    }
  }
	return false;
}

int myNode::searchDijkstra2(string start, int ans, int len, vector<string>& been, myNode& node, string finding) {
  if (len>ans) {
    return ans;
  } else {
    map<string, int> cons = node.getConnections();
    map<string, myNode*> cons2 = node.getConnections2();
    auto it2 = cons.begin();
    for (auto it=cons2.begin(); it != cons2.end(); it++) {
      if (it->first == finding) {
        if (ans > len+it2->second){
          ans = len+it2->second;
        }
      } else {
        if ((!vec_find(been, value+'-'+it->first)) && (it->first != start)) {
          ans = searchDijkstra2(start, ans, len+(it2->second), been, *(it->second), node.getValue());
        }
      }
      if (it2 != cons.end()) {
        it2++;
      }
    }
    return ans;
  }
}

int myNode::searchDijkstra(myNode node) {
  if (!(searchDepth(node))) {
    return -1;
  } else {
    int ans = -1;
    vector<string> been;
    auto it2 = connections.begin();
    for (auto it=connections2.begin(); it != connections2.end(); it++) {
      if (it->first == node.getValue()) {
        ans = it2->second;
      } else {
        been.push_back(value + '-' + it->first);
        been.push_back(it->first + '-' + value);
        ans = searchDijkstra2(value, ans, it2->second, been, *(it->second), node.getValue());
      }
      if (it2 != connections.end()) {
        it2++;
      }
    }
    return ans;
  }
}