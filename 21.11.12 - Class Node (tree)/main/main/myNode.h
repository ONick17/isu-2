#pragma once
#include <string>
#include <map>
#include <vector>
using namespace std;

class myNode {
public:
  myNode();
	myNode(string new_value);
  ~myNode();

  string getValue();

  void connect(myNode& node, int wei);
	void disconnect(myNode node);
  void print_connections();

  bool searchDepth(myNode node);
	int searchDijkstra(myNode node);

private:
	string value;
	map<string, int> connections;
  map<string, myNode*> connections2;
	bool searchDepth2(vector<string> been, myNode& node, string finding);
  int searchDijkstra2(string start, int ans, int len, vector<string>& been, myNode& node, string finding);

protected:
  map<string, int> getConnections();
  map<string, myNode*> getConnections2();
};
