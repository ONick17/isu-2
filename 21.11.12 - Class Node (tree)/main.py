class Node:
    def __init__(self, value):
        self.val = value
        self.cons = []

    def __len__(self):
        return(len(self.cons))

    def __str__(self):
        return(str(self.val))

    def add_connect(self, item):
        if type(item) == Node:
            if not(item in self.cons):
                self.cons.append(item)
                item.cons.append(self)
            else:
                raise ValueError("These Nodes are already connected.")
        else:
            raise TypeError("Node is needed.")

    def remove_connect(self, item):
        if type(item) == Node:
            if item in self.cons:
                item.cons.remove(self)
                self.cons.remove(item)
            else:
                raise ValueError("These Nodes are already disconnected.")
        else:
            raise TypeError("Node is needed.")

    def check_connect(self, item):
        if type(item) == Node:
            if item in self.cons:
                return(True)
            else:
                return(False)
        else:
            raise TypeError("Node is needed.")

    def change_value(self, value):
        self.val = value

    def clear(self):
        for i in self.cons:
            i.cons.remove(self)
        self.cons.clear()

    def connects(self):
        lst = []
        for i in self.cons:
            lst.append(i.val)
        return(lst)

a = Node(27)
b = Node("lol")
a.add_connect(b)
print(a.connects())
print(b.connects())
b.remove_connect(a)
print(a.connects())
print(b.connects())
#a.add_connect("check")
print(a)
