﻿#include <iostream>
#include <vector>
#include <iomanip>
using namespace std;

void prnt1(vector<vector<int>> &rec) {
    int n = rec.size();
    int m = rec[0].size();
    for (int i = 0; i < n; i++) {
        cout << n - i << "   ";
        for (int j = 0; j < m; j++) {
            cout << setw(3) << rec[i][j];
        }
        cout << endl;
    }
    cout << endl;
    cout << "    ";
    for (int i = 0; i < m; i++) {
        cout << setw(3) << i + 1;
    }
    cout << endl;
}

void prnt2(vector<vector<int>>& rec) {
    int n = rec.size();
    int m = rec[0].size();
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            cout << setw(3) << rec[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

void def2(vector<vector<int>>& rec, int X1, int X2, int Y1, int Y2, int* num) {
    int n = rec.size();
    int m = rec[0].size();
    while (X1 != X2 && Y1 != Y2) {
        if ((X1 > -1) && (X1 < m) && (Y1 > -1) && (Y1 < n)) {
            rec[Y1][X1] = *num;
            *num -= 1;
        }
        if (X2 > X1) {
            X1 += 1;
        }
        else {
            X1 -= 1;
        }
        if (Y2 > Y1) {
            Y1 += 1;
        }
        else {
            Y1 -= 1;
        }
    }
}

void def(vector<vector<int>>& rec, int xM, int yM) {
    int n = rec.size();
    int m = rec[0].size();
    int r = 1;
    int num = n * m - 1;
    int X1, Y1, X2, Y2;
    while (num > 0) {
        X1 = xM - r;
        Y1 = yM;
        X2 = xM;
        Y2 = yM - r;
        def2(rec, X1, X2, Y1, Y2, &num);
        X1 = X2;
        Y1 = Y2;
        X2 = xM + r;
        Y2 = yM;
        def2(rec, X1, X2, Y1, Y2, &num);
        X1 = X2;
        Y1 = Y2;
        X2 = xM;
        Y2 = yM + r;
        def2(rec, X1, X2, Y1, Y2, &num);
        X1 = X2;
        Y1 = Y2;
        X2 = xM - r;
        Y2 = yM;
        def2(rec, X1, X2, Y1, Y2, &num);
        r += 1;
    }
}

int main()
{
    setlocale(LC_ALL, "Russian");
    int n, m, xM, yM, i, j;
    cout << "Введите размер прямоугольника через пробел (ширина:высота): ";
    cin >> m >> n;
    vector<vector<int>> rec;
    rec.resize(n);
    for (i = 0; i < n; i++) {
        rec[i].resize(m);
        for (j = 0; j < m; j++) {
            rec[i][j] = 0;
        }
    }
    prnt1(rec);

    cout << "Введите координаты точки отсчёта через пробел (x:y): ";
    cin >> xM >> yM;
    xM -= 1;
    yM = n - yM;
    int nm = n * m;
    (rec[yM])[xM] = nm;
    prnt2(rec);

    def(rec, xM, yM);

    prnt2(rec);
    return 0;
}