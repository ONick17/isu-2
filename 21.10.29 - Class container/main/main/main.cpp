﻿#include <iostream>
#include <typeinfo>
#include <vector>
#include <string>
#include <cassert>
//#include <iterator>
using namespace std;

template <typename T> class Arr {
private:
    vector <T> vec;
    int N;
    typedef typename vector<T>::iterator iterator;
    typedef typename vector<T>::const_iterator const_iterator;

public:
    //Конструктор без аргументов
    Arr() {
        N = 0;
    }

    //Конструктор с длинной
    Arr(int n) {
        N = n;
        vec = {};
        for (int i = 0; i < N; i++) {
            vec.push_back(0);
        }
    }

    //Конструкторы с массивом
    template <size_t SZ>
    Arr(T(&lst)[SZ]) {
        N = SZ;
        for (int j = 0; j < N; j++) {
            vec.push_back(lst[j]);
        }
    }

    //Присвоение массива переменной
    Arr& operator=(initializer_list<T> il) {
        for (const auto& x : il)
            cout << x << " ";
        N = il.size();
        vec = il;
        return *this;
    }

    //Деструктор
    ~Arr() {

    }

    //Обращение по индексу
    T& operator[](const int i) {
        assert(i >= 0 && i < N);
        return vec[i];
    }

    //Вывод в cout
    /*template <class C>
    ostream& operator<<(ostream& os, Arr<C>& container) {
        os << "{ ";
        string vec_out = "";
        for (int i = 0; i < N; i++) {
            vec_out = vec_out + to_string(vec[i]) + ' ';
        }
        os << vec_out << '}';
        return os;
    }*/

    //Добавление в конец
        void append(T nw) {
        vec.push_back(nw);
        N++;
    }

    //Добавление по индексу
    void append(T nw, int ind) {
        N++;
        T temp;
        bool chk = true;
        for (int i=0; i<N; i++) {
            if (chk) {
                if (i==ind) {
                    temp = vec[i];
                    vec[i] = nw;
                    chk = false;
                };
            } else {
                nw = vec[i];
                vec[i] = temp;
                temp = nw;
            }
        }
    }

    //Удаление с конца
    void pop(){
        vec.pop_back();
        N--;
    }

    //Удаление по индексу
    void remove_index(int ind) {
        N--;
        T temp;
        bool chk = true;
        for (int i=0; i<N; i++) {
            if (chk) {
                if (i==ind) {
                    vec[i] = vec[i+1];
                    chk = false;
                };
            } else {
                temp = vec[i];
                vec[i] = vec[i+1];
                vec[i+1] = temp;
            }
        }
        vec.pop_back();
    }

    //Удаление по значению
    void remove(T rm) {
        N--;
        T temp;
        bool chk = true;
        for (int i=0; i<N; i++) {
            if (chk) {
                if (vec[i]==rm) {
                    vec[i] = vec[i+1];
                    chk = false;
                };
            } else {
                temp = vec[i];
                vec[i] = vec[i+1];
                vec[i+1] = temp;
            }
        }
        vec.pop_back();
    }

    //Поиск элемента
    int find(T val){
        for (int i=0; i<N; i++) {
            if (vec[i] == val) {
                return i;
            }
        }
        return -1;
    }

    //Обратный порядок
    void reverse(){
        T temp;
        for (int i = 0; i < N/2; i++) {
            temp = vec[i];
            vec[i] = vec[N-i-1];
            vec[N-i-1] = temp;
        }
    }

    //Размер
    int size() {
        return N;
    }

    //Вывод
    void print() {
        for (int i = 0; i < N; i++) {
            cout << vec[i];
            if (i != N-1) {
                cout << ' ';
            }
        }
        cout << endl;
    }

    //Сортировка
    void sort() {
        T temp;
        for (int i = 0; i < N; i++) {
            temp = vec[i];
            for (int j = i; j < N; j++) {
                if (temp > vec[j]) {
                    temp = vec[j];
                    vec[j] = vec[i];
                    vec[i] = temp;
                }
            }
        }
    }

    //Итераторы
    iterator begin(){
        return vec.begin();
    }
    iterator end(){
        return vec.end();
    }
    const_iterator begin() const{
        return vec.begin();
    }
    const_iterator end() const{
        return vec.end();
    }

    //Очистка
    void clear() {
      vec = {};
      N = 0;
    }

    //Вывод максимума
    T& max() {
        return min_max(false);
    }

    //Вывод минимума
    T& min() {
        return min_max(true);
    }

protected:
    //Поиск максимума/минимума
    string min_max(bool x) {
        T ansMin;
        T ansMax;
        T kekeMin = vec[0];
        T kekeMax = vec[0];
        for (int i = 1; i < N; i++) {
            if (kekeMin > vec[i]) {
                kekeMin = vec[i];
            }
            if (kekeMax < vec[i]) {
                kekeMax = vec[i];
            }
        }
        ansMin = kekeMin;
        ansMax = kekeMax;
        if (x) {
            return ansMin;
        }
        return ansMax;
    }
};

//Перегрузка для cout
template <typename T>
ostream& operator<<(ostream& os, Arr<T>& vec) {
    os << '{';
    /*if (typeid(vec[0]) == typeid(int)) {
        for (int i = 0; i < vec.size(); i++) {
            os << to_string(vec[i]);
            if (i != vec.size()-1){
                os << "; ";
            }
        }
    } else if (typeid(vec[0]) == typeid(string)) {
        for (int i = 0; i < vec.size(); i++) {
            os << vec[i];
            if (i != vec.size()-1){
                os << "; ";
            }
        }
    }*/
    for (int i = 0; i < vec.size(); i++) {
        os << to_string(vec[i]);
        if (i != vec.size()-1){
            os << "; ";
        }
    }
    os << '}';
    return os;
}

int main() {
    Arr <int> a(7);
    cout << a.size() << endl;
    a.print();
    cout << a[2] << endl;
    cout << endl;

    Arr <int> a1;
    cout << a1.size() << endl;
    a1.print();
    cout << endl;

    int b[] = { 1, 7, 3, 4, 5, 6, 7 };
    Arr <int> b1(b);
    cout << b1.size() << endl;
    b1.print();
    cout << b1[2] << endl;
    cout << endl;

    int c[] = { 1, 7, 7, 5, 6, 7 };
    b1 = c;
    cout << b1.size() << endl;
    b1.print();
    cout << b1[2] << endl;
    cout << endl;

    b1.sort();
    cout << b1.size() << endl;
    b1.print();
    cout << endl;

    string d[] = { "!the chemical brothers!", "!abba!", "!daft punk!"};
    Arr <string> b2(d);
    cout << b2.size() << endl;
    b2.print();
    cout << endl;

    b2.sort();
    cout << b2.size() << endl;
    b2.print();
    cout << endl;

    string e[] = {"!drive!", "!la la land!", "!the nice guys!", "!blade runner 2049!"};
    b2 = e;
    cout << b2.size() << endl;
    b2.print();
    cout << endl;

    b2.append("!the believer!");
    cout << b2.size() << endl;
    b2.print();
    cout << b2[2] << endl;
    cout << endl;

    b2.append("!only god forgives!", 2);
    cout << b2.size() << endl;
    b2.print();
    cout << endl;

    b2.remove_index(1);
    cout << b2.size() << endl;
    b2.print();
    cout << endl;

    b2.remove("!only god forgives!");
    cout << b2.size() << endl;
    b2.print();
    cout << endl;

    b2.pop();
    cout << b2.size() << endl;
    b2.print();
    //cout << b2 << endl;
    cout << endl;

    cout << b2.find("!the nice guys!") << endl;
    cout << endl;

    int f[] = { 1, 2, 3, 4, 5, 6, 7 };
    b1 = f;
    b1.print();
    b1.reverse();
    b1.print();
    cout << endl;

    b1 = f;
    b1.append(8);
    b1.print();
    b1.reverse();
    b1.print();
    cout << endl;

    std::vector<int>::iterator iter = b1.begin();
    cout << *(iter) << endl;
    iter++;
    cout << *(iter) << endl;
    iter = b1.end();
    iter--;
    cout << *(iter) << endl;
    iter--;
    cout << *(iter) << endl;
    cout << endl;

    cout << b1;

    return(0);
}