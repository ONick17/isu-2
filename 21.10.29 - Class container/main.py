class contain:
    def __init__(self, *kekes):
        self.kekes = []
        self.types = {"<class 'int'>": [], "<class 'str'>": [], "<class 'bool'>": [], "<class 'float'>": []}
        for i in kekes:
            self.types[str(type(i))].append(str(i))
            self.kekes.append(str(i))

    def __getitem__(self, index):
        if (index+1) > len(self.kekes):
            raise IndexError("index out of range")
        else:
            a = self.kekes[index]
            if a in self.types["<class 'int'>"]:
                return(int(self.kekes[index]))
            elif a in self.types["<class 'str'>"]:
                return(self.kekes[index])
            elif a in self.types["<class 'bool'>"]:
                return(bool(self.kekes[index]))
            elif a in self.types["<class 'float'>"]:
                return(float(self.kekes[index]))

    def __setitem__(self, index, item):
        if (index+1) > len(self.kekes):
            raise IndexError("index out of range")
        else:
            a = self.kekes[index]
            for i in self.types.keys:
                if a in self.types[i]:
                    self.types[i].remove(a)
                    break
            self.types[str(type(item))].append(str(item))
            self.kekes[index] = str(item)

    def __len__(self):
        return(len(self.kekes))

    def append(self, item):
        self.types[str(type(item))].append(str(item))
        self.kekes.append(str(item))

    def find(self, item):
        tp = str(type(item))
        if str(item) in self.types[tp]:
            num = 0
            for i in self.keys:
                if i == str(item):
                    return(num)
                num += 1
        else:
            return(-1)

    def sort(self):
        self.kekes.sort()

    def reverse(self):
        self.kekes = self.kekes.reverse()

    def clear(self):
        self.kekes.clear()
        self.types.clear()

    def __str__(self):
        return "class Contain: " + str(self.kekes)

a = contain()
a.append(1)
a.append("check")
print(a[0])
print(a)
