#функция перевода из 11-21 в 10
def sys16to10(num, sys):
    ltrsA1 = {'A': 10, 'B': 11, 'C': 12, 'D': 13, 'E': 14, 'F': 15, 'G': 16, 'H': 17, 'I': 18, 'J': 19, 'K': 20}
    num1 = 0
    chk = False
    if (num[0] == '-'):
        chk = True
        num = num[1:]
    stp = len(num) - 1
    for i in num:
        if i in list(ltrsA1.keys()):
            c = ltrsA1[i]
        else:
            c = int(i)
        num1 += c * (sys ** stp)
        stp -= 1
    num1 = str(num1)
    if chk:
        num1 = '-' + num1
    return(num1)

#функция перевода из 10 в 11-21
def sys10to16(num, sys):
    ltrs1A = {10: 'A', 11: 'B', 12: 'C', 13: 'D', 14: 'E', 15: 'F', 16: 'G', 17: 'H', 18: 'I', 19: 'J', 20: 'K'}
    num1 = ""
    chk = False
    if (num[0] == '-'):
        chk = True
        num = num[1:]
    num = int(num)
    while True:
        if num < sys:
            if num > 9:
                num1 += ltrs1A[num]
            else:
                num1 += str(num)
            break
        else:
            if num%sys > 9:
                num1 += ltrs1A[num%sys]
            else:
                num1 += str(num%sys)
            num //= sys
    num1 = num1[::-1]
    if chk:
        num1 = '-' + num1
    return(num1)

#функция перевода из 10 в 2-9
def sys10toX(num, sys):
    num1 = ""
    chk = False
    if (num[0] == '-'):
        chk = True
        num = num[1:]
    num = int(num)
    while True:
        if num < sys:
            num1 = num1 + str(num)
            break
        else:
            num1 = num1 + str(num%sys)
            num //= sys
    num = num1[::-1]
    if chk:
        num = '-' + num
    return(num)

#функция перевода из 2-9 в 10
def sysXto10(num, sys):
    num1 = 0
    stp = len(num) - 1
    chk = False
    if (num[0] == '-'):
        chk = True
        num = num[1:]
    for i in num:
        num1 += int(i) * (sys ** stp)
        stp -= 1
    num1 = str(num1)
    if chk:
        num1 = '-' + num1
    return(num1)

#функция изменения системы счисления
def baseb(num, sys1, sys2):
    #на случай отрицательных чисел 1
    chk = False
    if (num[0] == '-'):
        chk = True
        num = num[1:]
    #перевод в 10
    if (sys1 < 10):
        num = sysXto10(num, sys1)
    elif (sys1>10) and (sys1<22):
        num = sys16to10(num, sys1)
    #перевод из 10
    if (sys2 < 10):
        num = sys10toX(num, sys2)
    elif (sys2>10) and (sys2<22):
        num = sys10to16(num, sys2)
    #на случай отрицательных чисел 2
    if chk:
        num = '-' + num
    return(num)

#функция изменения системы на 10
def systo10(val1, val2, sys1, sys2):
    if (sys1 < 10):
        ans1 = int(baseb(val1, sys1, 10))
    elif (sys1>10) and (sys1<22):
        ans1 = int(baseb(val1, sys1, 10))
    elif (sys1 == 10):
        ans1 = int(val1)
    if (sys2 < 10):
        ans2 = int(baseb(val2, sys2, 10))
    elif (sys2>10) and (sys2<22):
        ans2 = int(baseb(val2, sys2, 10))
    elif (sys2 == 10):
        ans2 = int(val2)
    return(ans1, ans2)

#функция изменения 10 системы
def systoX16(ans, sys):
    if (sys < 10):
        ans = baseb(ans, 10, sys)
    elif (sys>10) and (sys<22):
        ans = baseb(ans, 10, sys)
    return(ans)

#функция создания переменной класса cl_chislo
def chislo(val, sys):
    val = str(val)
    sys = int(sys)
    if (sys<2) or (sys>21):
        print("Данная система счисления не обрабатывается!")
        print("Принимаются только системы от 2 до 21 включительно. Присвоена десятичная система.")
        sys = 10
    val = val.upper()
    ltrsA1 = {'A': 10, 'B': 11, 'C': 12, 'D': 13, 'E': 14, 'F': 15, 'G': 16, 'H': 17, 'I': 18, 'J': 19, 'K': 20}
    ltrs1A = {10: 'A', 11: 'B', 12: 'C', 13: 'D', 14: 'E', 15: 'F', 16: 'G', 17: 'H', 18: 'I', 19: 'J', 20: 'K'}
    nms = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    chk = False
    for zhopa in range(len(val)):
        if val[zhopa] in list(ltrs1A.values()):
            if (ltrsA1[val[zhopa]] >= sys):
                print("В числе", val, "обнаружена неверная для системы счисления ("+str(sys)+") цифра (" + val[zhopa] + ").")
                print("Цифра заменена на наибольшую в данной системе.")
                chk = True
                if (sys<11):
                    val = val[:zhopa]+str(sys-1)+val[zhopa+1:]
                else:
                    val = val[:zhopa]+ltrs1A[sys-1]+val[zhopa+1:]
        elif val[zhopa] in nms:
            if (int(val[zhopa]) >= sys):
                print("В числе", val, "обнаружена неверная для системы счисления ("+str(sys)+") цифра (" + val[zhopa] + ").")
                print("Цифра заменена на наибольшую в данной системе.")
                chk = True
                if (sys<11):
                    val = val[:zhopa]+str(sys-1)+val[zhopa+1:]
                else:
                    val = val[:zhopa]+ltrs1A[sys-1]+val[zhopa+1:]
        elif (val[zhopa] != '-'):
            print("В числе", val, "обнаружена неверная для системы счисления ("+str(sys)+") цифра (" + val[zhopa] + ").")
            print("Цифра заменена на наибольшую в данной системе.")
            chk = True
            if (sys<11):
                val = val[:zhopa]+str(sys-1)+val[zhopa+1:]
            else:
                val = val[:zhopa]+ltrs1A[sys-1]+val[zhopa+1:]
    if chk:
        print("Новое число: " + val + "(" + str(sys) + ")")
    a = cl_chislo(val, sys)
    return(a)

#класс "число"
class cl_chislo:
    def __init__(self, val, sys = 10):
        if (type(val) != str):
            val = str(val)
        self.val = val
        self.sys = sys
    #функция вывода
    def __str__(self):
        return(str(self.val)+"("+str(self.sys)+')')
    #функция сложения
    def __add__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                ans = ans1 + ans2
                ans = str(ans)
                ans = systoX16(ans, self.sys)
                ans = chislo(ans, self.sys)
                return(ans)
            else:
                ans = chislo(int(self.val) + int(other.val), self.sys)
                return(ans)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция вычитания
    def __sub__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                ans = ans1 - ans2
                ans = str(ans)
                ans = systoX16(ans, self.sys)
                ans = chislo(ans, self.sys)
                return(ans)
            else:
                ans = chislo(int(self.val) - int(other.val), self.sys)
                return(ans)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция умножения
    def __mul__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                ans = ans1 * ans2
                ans = str(ans)
                ans = systoX16(ans, self.sys)
                ans = chislo(ans, self.sys)
                return(ans)
            else:
                ans = chislo(int(self.val) * int(other.val), self.sys)
                return(ans)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция деления с остатком
    def __truediv__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                ans = ans1 / ans2
                ans = float('{:.1f}'.format(ans))
                ans = str(ans)
                ans = systoX16(ans, self.sys)
                ans = chislo(ans, self.sys)
                return(ans)
            else:
                ans = int(self.val) / int(other.val)
                ans = float('{:.1f}'.format(ans))
                ans = chislo(ans, self.sys)
                return(ans)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция деления нацело
    def __floordiv__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                ans = ans1 // ans2
                ans = str(ans)
                ans = systoX16(ans, self.sys)
                ans = chislo(ans, self.sys)
                return(ans)
            else:
                ans = chislo(int(self.val) // int(other.val), self.sys)
                return(ans)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция поиска остатка от деления
    def __mod__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                ans = ans1 % ans2
                ans = str(ans)
                ans = systoX16(ans, self.sys)
                ans = chislo(ans, self.sys)
                return(ans)
            else:
                ans = chislo(int(self.val) % int(other.val), self.sys)
                return(ans)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция возведения в степень
    def __pow__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                ans = ans1 ** ans2
                ans = str(ans)
                ans = systoX16(ans, self.sys)
                ans = chislo(ans, self.sys)
                return(ans)
            else:
                ans = chislo(int(self.val) ** int(other.val), self.sys)
                return(ans)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция поиска меньшего
    def __lt__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                return(ans1 < ans2)
            else:
                return(self.val < other.val)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция поиска меньшего или равного
    def __le__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                return(ans1 <= ans2)
            else:
                return(self.val <= other.val)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция поиска равного
    def __eq__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                return(ans1 == ans2)
            else:
                return(self.val == other.val)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция поиска неравного
    def __ne__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                return(ans1 != ans2)
            else:
                return(self.val != other.val)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция поиска большего
    def __gt__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                return(ans1 > ans2)
            else:
                return(self.val > other.val)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError
    #функция поиска большего или равного
    def __ge__(self, other):
        if (type(other) == cl_chislo):
            if (self.sys != 10) or (other.sys != 10):
                ans1, ans2 = systo10(self.val, other.val, self.sys, other.sys)
                return(ans1 >= ans2)
            else:
                return(self.val >= other.val)
        elif (str(type(other)) != "<class 'NoneType'>"):
            print("Операции с другим типом чисел невозможны для класса «число»")
            raise TypeError

#тестовые данные
a = chislo(43, 16) #67
b = chislo("5", 10) #5
c = chislo(101, 8) #65
e = chislo(50505, 6) #6665
e2 = chislo("1a09", 16) #6665
f = chislo("I8.F", 19) #6665
g = chislo("18d6", 19) #10000
er = chislo("18d6", 10) #вызов ошибки

lst = [a, b, c, e] #67, 5, 65, 6665 (43, 5, 101, 50505)
lst.sort()
for i in lst:
    print(i, end=' ') #5, 65, 67, 6665 (5, 101, 43, 50505)
print()

if (a > c): #67 > 65
    print(a)
elif (a < c): #67 < 65
    print(c)
elif (a == c):
    print(a, c)

keke = e2 + a #6665 + 67 = 6732 (1A4C)
print(keke)

if (f == e): #6665 == 6665
    print("YES")

keke = f + a #6665 + 67 = 6732 (IC6)
print(keke)

keke = f - g #6665 - 10000 = -3335 (-94A)
print(keke)

keke = chislo(1, 10) - chislo(2, 10) #1 - 2 = -1
print(keke)

keke = chislo(-5, 20) + chislo("-FF", 20)
print(chislo(-5, 20))
print(chislo("-FF", 20))
print(keke)

keke = chislo(-5, 10) + chislo(-7, 10)
print(keke)

#ввод пользователем
while True:
    print("Желаете ввести данные вручную?")
    mode = input("Y/N ")
    if (mode == "Y"):
        chk1 = True
        break
    elif (mode == "N"):
        chk1 = False
        break
    else:
        print("Ввод нарушен. Повторите попытку.")

while chk1:
    print()
    print("Введите через пробел первое число и его систему счисления:")
    x, sys = map(str, input().split())
    x = chislo(x, sys)
    if x == None:
        print("Число отсутствует (None). Операция отменена.")
    else:
        print("Введите через пробел второе число и его систему счисления:")
        y, sys = map(str, input().split())
        y = chislo(y, sys)
        if y == None:
            print("Число отсутствует (None). Операция отменена.")
        else:
            print("Выберите операцию между двумя числами:")
            print("1 - сумма")
            print("2 - вычитание")
            print("3 - умножение")
            print("4 - деление с остатком")
            print("5 - деление нацело")
            print("6 - поиск остатка от деления")
            print("7 - возведение в степень")
            print("8 - сравнение и вывод большего (или обоих чисед, если они равны)")
            mode = input()
            if(mode == "1"):
                print(x+y)
            elif(mode == "2"):
                print(x-y)
            elif(mode == "3"):
                print(x*y)
            elif(mode == "4"):
                print(x/y)
            elif(mode == "5"):
                print(x//y)
            elif(mode == "6"):
                print(x%y)
            elif(mode == "7"):
                print(x**y)
            elif(mode == "8"):
                if (x>y):
                    print(x)
                elif (y>x):
                    print(y)
                else:
                    print(x, y)
            else:
                print("Неверный ввод. Операция отменена.")
            chk2 = True
            while chk2:
                print("Продолжить?")
                mode = input("Y/N: ")
                if(mode == "Y"):
                    chk2 = False
                elif(mode == "N"):
                    chk1 = False
                    chk2 = False
                else:
                    print("Неверный ввод")
                
