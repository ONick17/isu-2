from tkinter import filedialog
from tkinter import *

root = Tk()
root.withdraw()
fl = filedialog.askopenfile(mode="r")
ln = fl.readline()
lst = []
while ln:
    if str(ln[-1]) == '\n':
        lst.append(str(ln[:-1]))
    else:
        lst.append(str(ln))
    ln = fl.readline()
fl.close()
lst.sort()
lstnew = []
dic = {}
chk = (lst[0])[0]
for i in lst:
    if i[0] == chk:
        lstnew.append(i)
    else:
        dic[chk] = lstnew.copy()
        lstnew.clear()
        chk = i[0]
        lstnew.append(i)
dic[chk] = lstnew.copy()
for i in dic:
    print(dic[i])
